#-------------------------------------------------
#
# Project created by QtCreator 2020-05-01T23:07:54
#
#-------------------------------------------------

QT       += core gui charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Studnia_potencjalu
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    basicmath.cpp

HEADERS  += mainwindow.h \
    quantumwell.h \
    mui_mainlayout.h \
    basicmath.h

FORMS    += mainwindow.ui
