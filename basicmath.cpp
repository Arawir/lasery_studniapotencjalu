#include "basicmath.h"
#include <cassert>
#include <algorithm>

double q = 1.602e-19;
double m0 = 9.1e-31;
double hbar = 1.05459e-34;
double g0 = 2.0*m0/hbar/hbar*(1.0e-3*q)*1.0e-20;

void normalize(std::vector<double>& nVector)
{
    double lSum =0.0;
    for(auto& value : nVector){
        lSum += pow(value,2);
    }

    for(uint i=0; i<nVector.size(); i++){
        nVector[i] /= lSum;
    }
}

void zeroes(std::vector<double>& nVector)
{
    for(uint i=0; i<nVector.size(); i++){
        nVector[i] = 0.0;
    }
}

double findMinPotential(const std::vector<double>& v)
{
    auto iter = std::min_element(v.begin(),v.end());
    return *iter;
}

int findPositionOfMinPotential(const std::vector<double>& v)
{
    auto iter = std::min_element(v.begin(),v.end());
    return iter-v.begin();
}

double findMaxPotential(const std::vector<double>& v)
{
    auto iter = std::max_element(v.begin(),v.end());
    return *iter;
}

std::vector<Eigen> martinDean(std::vector<double> v,
                std::vector<double> m,
                int number,
                double delta)
{
    assert(v.size()==m.size());
    int k;
    double potmin,potmax;

    delta /= 1.0e-10;

    potmin = findMinPotential(v);
    potmax = findMaxPotential(v);
    k = findPositionOfMinPotential(v);

    potmin *= delta*delta*g0/q*1.0e3;
    potmax *= delta*delta*g0/q*1.0e3;

    std::vector<Eigen> oEigens;
    oEigens.resize(number);

    double** d = prepareMatrix(v.size(),m,v,delta);
    std::vector<double> enn = calculateEnergies(number,potmax,potmin,d,v.size()-1,v.size(),potmax,delta,oEigens);
    calculateEigenfunctions(number,enn,delta,k,v.size(),d,oEigens);

    return oEigens;
}

double** prepareMatrix(uint nz, std::vector<double> m, std::vector<double> v, double delta)
{
    double w1,w2;
    double** d;

    d = new double*[nz];
    for(uint i=0; i<nz; i++){
        d[i] = new double[2];
    }

    for(uint i=0; i<nz-1;i++){
        w1=1.0/m[i]*m0;
        w2=1.0/m[i+1]*m0;
        d[i][1]=w1+w2;
        d[i][1] += delta*delta*g0*v[i]/q*1.0e3;
        d[i+1][0] = -w2;
    }

    d[nz-1][1]=d[nz-2][1];
    d[nz-1][0]=d[nz-2][0];

    return d;
}



std::vector<double> calculateEnergies(const int number, double x1, double x0, double** d,
                       const int& km, const int& nz, const double& potmax, const double& delta,
                       std::vector<Eigen>& nEigens)
{
    std::vector<double> enn;
    enn.resize(number);
    int i, n,nu;
    double x,xk,dx,xw,y1,y2;
    double dok = 1.0e-24;

    for(i=1;i<number+1;i++){
        nu=1;
        while(1){
            xk=0.0;
            nu += 1;
            if(nu>100){ break; }
            xw=(float)i;
            dx=x1-x0;
            if(dx<dok){ break; }
            x=(x0+x1)/2.0;
            y1=d[0][1]-x;
            if(y1<=0.0){
                xk += 1.0;
                if(xk>=xw){
                    dx=x1-x0;
                    if(dx<dok){ break; }
                    x1=x;
                    continue;
                }
            }

            for(n=1;n<km;n++){
                y2=d[n][1]-x-d[n][0]*d[n][0]/y1;
                if(y2<=0.0){
                    xk += 1.0;
                    if(xk>=xw){
                        dx=x1-x0;
                        if(dx<dok){ break; }
                        x1=x;
                        continue;
                    }
                }
                y1=y2;
            }

            y2=d[nz-1][1]-x-d[nz-1][0]*d[nz-1][0]/y1;
            if(y2<=0.0){
                xk += 1.0;
                if(xk>xw){
                    dx=x1-x0;
                    if(dx<dok){ break; }
                    x1=x;
                    continue;
                }
            }
            dx=x1-x0;
            if(dx<dok){ break; }
            x0=x;
        }

        x0=0.5*(x+x0);
        x1=potmax;
        enn[i-1]=x0/delta/delta/g0;

        nEigens[i-1].value = enn[i-1]*q*1.0e-3;
    }

    return enn;
}

void calculateEigenfunctions(int number, std::vector<double>& enn, double delta,
                             int k, long long int nz, double** d, std::vector<Eigen>& nEigens )
{
    for(int ii=0; ii<number; ii++){
        double energia=enn[ii]*delta*delta*g0;
        double max_psi=1.0;
        int i0=k-1.0;
        double del1_m[nz];
        double del_p[nz];
        std::vector<double> psi_dwsz;
        psi_dwsz.resize(nz);

        zeroes(psi_dwsz);

        while(i0 != k){
            i0=k;
            psi_dwsz[i0]=10.0;
            del1_m[0]=1.0/(d[0][1]-energia);
            del_p[nz-1]=1.0/(d[nz-1][1]-energia);

            for(int j=1;j<nz;j++){
                del1_m[j]=1.0/(d[j][1]-energia-pow(d[j][0],2)*del1_m[j-1]);
            }

            for(int j=1;j<nz-1;j++){
                del_p[nz-j-1]=1.0/(d[nz-j-1][1]-energia-d[nz-j][0]*d[nz-j][0]*del_p[nz-j]);
            }

            for(int j=i0;j<nz-1;j++){
                psi_dwsz[j+1]=-d[j+1][0]*del_p[j+1]*psi_dwsz[j];
                if(psi_dwsz[j+1]>max_psi){
                    max_psi=psi_dwsz[j-1];
                    k=j+1;
                }
            }

            for(int j=i0;j>=1; j--){
                psi_dwsz[j-1]=-d[j][0]*del1_m[j-1]*psi_dwsz[j];
            }
        }

        normalize( psi_dwsz);
        for(int i=0;i<nz;i++){
            nEigens[ii].vector.push_back(psi_dwsz[i]);
        }
    }
}
