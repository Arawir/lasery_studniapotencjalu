#ifndef BASICMATH_H
#define BASICMATH_H

#include <cstdlib>
#include <iostream>
#include <cmath>
#include <fstream>
#include <stdio.h>
#include <vector>

using namespace std;

extern double q;
extern double m0;
extern double hbar;
extern double g0;

struct Eigen{
    double value;
    std::vector<double> vector;

    Eigen( double nValue, std::vector<double> nVector) :
        value{ nValue }
      , vector{ nVector } { }
    Eigen() = default;
};

void normalize(std::vector<double>& nVector);
void zeroes(std::vector<double>& nVector);
double findMinPotential(const std::vector<double> &v);
int findPositionOfMinPotential(const std::vector<double> &v);
double findMaxPotential(const std::vector<double> &v);

std::vector<Eigen> martinDean(std::vector<double> v,
                std::vector<double> m,
                int number,
                double delta);

double** prepareMatrix(uint nz, std::vector<double> m, std::vector<double> v, double delta);


std::vector<double> calculateEnergies(const int number, double x1, double x0, double** d,
                       const int& km, const int& nz, const double& potmax, const double& delta,
                       std::vector<Eigen>& nEigens);

void calculateEigenfunctions(int number, std::vector<double>& enn, double delta,
                             int k, long long int nz, double** d, std::vector<Eigen>& nEigens );
#endif // BASICMATH_H
