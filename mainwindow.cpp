#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    mMainLayout = new Mainlayout{ };
    setCentralWidget(mMainLayout);
}

MainWindow::~MainWindow()
{
    delete ui;
}
