#ifndef MUI_MAINLAYOUT
#define MUI_MAINLAYOUT


#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include "quantumwell.h"
#include <QDebug>

using namespace QtCharts;

class Mainlayout : public QWidget
{
    Q_OBJECT
public:
    Core::ParabolicQuantumWell mWell;
    QVBoxLayout* mLayout1;
    QHBoxLayout* mLayout2;


    QLineEdit* mParamAEdit;
    QLineEdit* mNumberEdit;
    QPushButton* mUpdateButton;

    QChart* mPotentialChart;
    QChartView* mPotetnialChartView;
    QChart* mFunctionChart;
    QChartView* mFunctionChartView;

public:
    Mainlayout( ) :
        QWidget{}
    {
        mLayout1 = new QVBoxLayout{ };
        setLayout(mLayout1);

        mLayout2 = new QHBoxLayout{ };
        mLayout1->addLayout(mLayout2);

        mParamAEdit = new QLineEdit{ };
        mParamAEdit->setPlaceholderText("Parameter k");
        mLayout2->addWidget(mParamAEdit);

        mNumberEdit = new QLineEdit{ };
        mNumberEdit->setPlaceholderText("N");
        mLayout2->addWidget(mNumberEdit);

        mUpdateButton = new QPushButton{ "Update" };
        mUpdateButton->setMaximumWidth(150);
        connect(mUpdateButton, SIGNAL(clicked(bool)), this, SLOT(update()));
        mLayout2->addWidget(mUpdateButton);




        mPotentialChart = new QtCharts::QChart();
        mPotentialChart->createDefaultAxes();

        mPotetnialChartView = new QtCharts::QChartView(mPotentialChart);
        mPotetnialChartView->setRenderHint(QPainter::Antialiasing);
        mLayout1->addWidget(mPotetnialChartView);

        mFunctionChart = new QtCharts::QChart();
        mFunctionChartView = new QtCharts::QChartView(mFunctionChart);
        mFunctionChartView->setRenderHint(QPainter::Antialiasing);
        mLayout1->addWidget(mFunctionChartView);

    }

private slots:
    void update()
    {
        mWell.xRange = 5.0;
        mWell.xDelta = 0.1;
        mWell.n = mNumberEdit->text().toInt();
        mWell.k = q;

        mWell.prepare();
        //mWell.calulateAnaliticalEigens();
        mWell.calculateNumericalEigens();

        updatePotentialGraph();
       // updateAnaliticalGraph();
        updateNumericalGraph();

    }

private:
    void updatePotentialGraph()
    {
        QLineSeries* mPotSeries = new QLineSeries{ };

        for(uint i=0; i<mWell.mX.size(); i++)
        {
            mPotSeries->append( mWell.mX[i], mWell.mV[i]/q );
        }

        mPotentialChart->removeAllSeries();
        mPotentialChart->addSeries(mPotSeries);
        mPotentialChart->createDefaultAxes();
        //mPotentialChart->axisX()->setTitleText("Z [A]");
        //mPotentialChart->axisY()->setTitleText("E [eV]");
    }

    void updateAnaliticalGraph()
    {
        QLineSeries* mAnSeries = new QLineSeries{ };

        for(uint i=0; i<mWell.mX.size(); i++)
        {
            mAnSeries->append( mWell.mX[i], mWell.mAnaliticalEigens[mWell.n-1].vector.at(i) );
        }

        mFunctionChart->removeAllSeries();
        mFunctionChart->addSeries(mAnSeries);
        mFunctionChart->createDefaultAxes();
    }

    void updateNumericalGraph()
    {
        QLineSeries* mNumSeries = new QLineSeries{ };

        for(uint i=0; i<mWell.mX.size(); i++)
        {
            mNumSeries->append( mWell.mX[i], mWell.mNumericalEigens[mWell.n-1].vector.at(i) );
        }
        qDebug() << "En: " << mWell.mNumericalEigens[mWell.n-1].value;

        mFunctionChart->removeAllSeries();
        mFunctionChart->addSeries(mNumSeries);
        mFunctionChart->createDefaultAxes();
    }

};

#endif // MUI_MAINLAYOUT

