#ifndef QUANTUMWELL
#define QUANTUMWELL

#include "basicmath.h"

namespace Core{

    class ParabolicQuantumWell
    {
    public:
        std::vector<double> mX;
        std::vector<double> mV;
        std::vector<double> mM;

        std::vector<Eigen> mAnaliticalEigens;
        std::vector<Eigen> mNumericalEigens;

    public:
        double xRange, xDelta, k;
        uint n;

    public:
        ParabolicQuantumWell()
        {

        }
        ~ParabolicQuantumWell() = default;

        void prepare()
        {
            double x = -xRange;
            while(x<xRange+xDelta/2.0){
                mX.push_back(x);
                mV.push_back( k*pow(x,2));
                mM.push_back( m0 );
                x+=xDelta;
            }
        }

        void calulateAnaliticalEigens()
        {
            for(uint i=0; i<n; i++){
                mAnaliticalEigens.push_back( {analiticalEnergy(i), analiticalVector(i)} );
            }
        }

        double analiticalEnergy(uint n)
        {
            return hbar*2.0*k/m0*(0.5+(double)n);
            //return 2.0*k*(0.5+(double)n) * 4.3597e-18;
        }

        std::vector<double> analiticalVector(uint n)
        {
            double m0 = 1.0;
            double hbar = 1.0;

            std::vector<double> oVector;

            double x=-xRange;
            while( x<(xRange+xDelta/2.) ){
                double tA = 1.0/sqrt(pow(2.0,n)*factorial(n));
                double tB = pow( (2.0*k/q/m0/M_PI), 0.25);
                double tC = exp( -(2.0*k/q/m0*x*x)/(2.0*hbar) );
                double tPsi = tA*tB*tC*HermitePolynominal(n, x);
                oVector.push_back(tPsi);
                x+=xDelta;
            }

            return oVector;
        }


        double HermitePolynominal(uint n, double x)
        {
            if(n==0) return 1.0;
            if(n==1) return 2.0*x;
            return 2.0*x*HermitePolynominal(n-1,x) - 2*(n-1)*HermitePolynominal(n-2,x);
        }

        double factorial(uint n)
        {
            if(n==0) return 1.0;
            if(n==1) return 1.0;
            return (double)n*factorial(n-1);
        }

        void calculateNumericalEigens()
        {
            mNumericalEigens = martinDean(mV,mX,n,xDelta*1e-9);
        }



        void test()
        {
            k=q;
            n=8;
            xDelta=0.1;
            xRange=5.0;

            prepare();
            calulateAnaliticalEigens();
            calculateNumericalEigens();

            for(uint i=0;i<n;i++){
                cout << i << " "
                     << "N: " << mNumericalEigens[i].value/mNumericalEigens[0].value << " "
                     << "A: " << mAnaliticalEigens[i].value/mAnaliticalEigens[0].value << " "
                     << std::endl;
            }

//            std::vector<double> v;
//            std::vector<double> m;

//            double dz,x;



//            uint nz=1000;
//            n=8;
//            k=q;

//            double* energie = new double[n];
//            double** funkcje = new double*[nz];
//            for(uint i=0; i<nz;i++){
//                funkcje[i] = new double[n];
//            }

//            m.resize(nz);
//            v.resize(nz);

//            for(uint i=0;i<nz;i++){
//                m[i]=m0; //masa w kg
//                x = -5.0+10.0*(float)i/nz; //dyskretyzacja
//                v[i]=k*x*x; //przeliczenie na J
//            }

//            dz=10.0/(float)nz;
//            dz*=1e-9; //przeliczenie na m

//            martinDean(v,m,n,funkcje,energie, dz,nz);

//            for(uint i=0;i<n;i++){
//                cout << "N: " << i << " "
//                     //<< energie[i]/energie[0] << " | "
//                     << energie[i] << " | "
//                     //<< "A: " << mAnaliticalEigens[i].value/mAnaliticalEigens[0].value
//                     << "A: " << analiticalEnergy(i)
//                     << std::endl;
//            }

////            fstream plik.open("funkcja.dat",ios::out);

////            for(uint i=0; i<nz; i++){
////                plik << dz*(double)nz << " "
////                     << funkcje[i][0] << endl;
////            }
////            plik.close();
        }




    };

}

#endif // QUANTUMWELL

